package com.hibegin.simplewebserver.performance;

import com.hibegin.http.server.WebServerBuilder;
import com.hibegin.http.server.config.ServerConfig;

import java.util.concurrent.Executors;

public class Application {

    public static void main(String[] args) throws InterruptedException {
        ServerConfig serverConfig = new ServerConfig();
        serverConfig.getRouter().addMapper("/", DemoController.class);
        serverConfig.setRequestExecutor(Executors.newFixedThreadPool(5));
        new WebServerBuilder.Builder().serverConfig(serverConfig).build().startWithThread();
    }

}
