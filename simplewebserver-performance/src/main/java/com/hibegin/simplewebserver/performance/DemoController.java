package com.hibegin.simplewebserver.performance;

import com.hibegin.http.server.web.Controller;

public class DemoController extends Controller {
    public void index() {
        helloWorld();
    }

    public void helloWorld() {
        getResponse().renderText("Hello world");
    }
}
