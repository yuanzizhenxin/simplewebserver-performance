package tio.httpserver.performance;

import ch.qos.logback.classic.LoggerContext;
import org.slf4j.ILoggerFactory;
import org.slf4j.LoggerFactory;
import org.tio.http.common.HttpConfig;
import org.tio.http.common.HttpRequest;
import org.tio.http.common.HttpResponse;
import org.tio.http.common.RequestLine;
import org.tio.http.common.handler.HttpRequestHandler;
import org.tio.http.server.HttpServerStarter;

import java.io.IOException;

public class Application {

    public static void main(String[] args) throws IOException {
        ch.qos.logback.classic.LoggerContext loggerContext = (LoggerContext) org.slf4j.LoggerFactory.getILoggerFactory();
        loggerContext.stop();
        final HttpConfig httpConfig = new HttpConfig(6090, -1L, "/", "");
        HttpServerStarter httpServerStarter = new HttpServerStarter(httpConfig, new HttpRequestHandler() {
            @Override
            public HttpResponse handler(HttpRequest httpRequest) throws Exception {
                HttpResponse httpResponse = new HttpResponse(httpRequest, httpConfig);
                httpResponse.setBody("Hello world".getBytes(), httpRequest);
                return httpResponse;
            }

            @Override
            public HttpResponse resp404(HttpRequest httpRequest, RequestLine requestLine) {
                return null;
            }

            @Override
            public HttpResponse resp500(HttpRequest httpRequest, RequestLine requestLine, Throwable throwable) {
                return null;
            }

            @Override
            public void clearStaticResCache(HttpRequest httpRequest) {

            }
        });
        httpServerStarter.start();
    }
}
